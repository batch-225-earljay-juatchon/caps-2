const User = require("../models/user");

const bcrypt = require("bcrypt"); 
const auth = require("../auth");
const user = require("../models/user");


module.exports.userRegister = (reqBody) => {


	let newUser = new User({

		// firstName : reqBody.firstName,
		// lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	})


	return newUser.save().then((user, error) => {
		if (error) {

			return false;

		} else {

			return user
		};
	});
}; 




module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return {
				message: "Not found in our database"
			}

		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);


			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message: "password was incorrect"
				}
			};

		};

	});
};


module.exports.checkUser = async function (reqBody) {
    try {
        let result = await User.find({
            email: reqBody.email
        })

        if (result.length > 0) {

            return true
        } else {

            return false
        }
    } catch (error) {
        return { message: "ERROR!" }
    }
}


 //User Details
module.exports.retrieveUserById = async function (userId) {
    // if (!data.isAdmin) {
    //     let message = Promise.resolve('User must be Admin to Access this.')

    //     return message.then((value) => {
    //         return value
    //     })
    // }

    try {
        let result = await User.findOne({ _id: userId })
        if (result == null) {
            return { message: "Invalid ID" }
        } else {
            return result
        }
    } catch {
        return { message: "Error!" }
    }

}
// // Retrieve User Details
// module.exports.getProfile = (userData) => {
// 	return User.findOne({id : userData.id}).then(result => {
// 		if (result == null){
// 			return false
// 		} else {
// 			result.password = ""
// 			return result
// 		}
// 	});
// };
