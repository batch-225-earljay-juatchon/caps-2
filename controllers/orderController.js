const Order = require("../models/order");
const Product = require('../models/product');
const User = require('../models/user')


// Placing an Order directly  Users only
// module.exports.checkoutProduct = async (reqBody, userId, data) => {

//   try {
   
//      if (data.isAdmin == false) {
      
//       let totalAmount = await TotalAmount(reqBody);
      
//       const Ordered = new Order({

//             userId: userId,
//             products: reqBody.products,
//             totalAmount: totalAmount
//       });
      
//     const newOrder =  await Ordered.save();

//       return { newOrder,
//             message: 'Order Created Successfully!'
//       };

//     } else {

//       return {
//             message: "Admin is not allowed!"
//       }
//     }
//   } catch (err) {

//       return err;

//   };
// };

// // Getting the total amount
// const TotalAmount = async (data) => {

//      if (data.products.length === 0) {

//          return 0;
//   };

//   let amount = 0;

//       for (const product of data.products) {

//         try {
//             const foundProduct = await Product.findById(product.productId);
//              amount += foundProduct.price * product.quantity;
//     }   catch (err) {

//       return err;

//     };
//   };
  
//   return amount;
// };


// Creating Order

module.exports.createOrder = async (product, data) => {

	let findProduct = await Product.findById(product);

	let findUser = await User.findById(data.userId);

	if(findUser.isAdmin !== true){

		let newOrder = new Order({

			userId : findUser.id,
			products : [{
				productId : findProduct.id,
				name : findProduct.name,
				price : findProduct.price}],
			totalAmount : findProduct.price
		}) 	
			return newOrder.save(); 

	} else {

		return {
			message : "Admins are not allowed to create order!"
		}
	}

}

//Check Out
module.exports.getCheckOut = async (product, data) => {
  let findOrders = await Order.find({userId : data.userId});

  const result = () => {
    for (let i = 0; i < findOrders.length; i++) {
      let x = findOrders[i].products;
      for (let j = 0; j < x.length; j++) {
        let y = x[j].productId;
        if (y === product) {
          return findOrders[i];
        }
      }
    }
    return false;
  };

  if (findOrders.length > 0) {
    return result();
  } else {
    return false;
  }
};

module.exports.checkOut = async (orderId, data) => {

	let findUser = await User.findById(data.userId);

	let order = await Order.findById(orderId);
  let product = await order.products;

	if(findUser.isAdmin !== true){

		findUser.orders.push({
      orderId : orderId,
      products : order.products,
      totalAmount : order.totalAmount
    });

		return findUser.save();

	} else {

		return {
			message : "Admin go away!"
		}
	}
}