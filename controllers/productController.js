const Product = require("../models/product");


module.exports.addProduct = (data) => {
	// if (data.isAdmin) {
		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			color: data.product.color,
			quantity: data.product.quantity,
			price: data.product.price,

		});

		return new_product.save().then((new_product, error) => {
			if (error) {
				return error
			}

			return {
				message: 'New product successfully created!'
			}
		});
	// };

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	});

};

module.exports.retrieveAllProducts = (data) => {

	 // if (data.isAdmin) {



		return Product.find({}).then(result => {

			return result;
		})

	 // } else {

		let message = Promise.resolve('User must me Admin to Access this.')

		return message.then((value) => {
			return value

		});

	 // };
};


module.exports.retrieveAllActiveProducts = async () => {


	 let result = await Product.find({ isActive: true })
		return result;
	

};


module.exports.retrieveSingleProduct = (productId) => {

	return Product.findById(productId).then(result => {

		return result;
	});

};


module.exports.updateProduct = async (reqParams, reqBody, data) => {
	// if (data.isAdmin) {

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			quantity: reqBody.quantity,
			color: reqBody.color,
			price: reqBody.price
		};

		  await Product.findByIdAndUpdate(reqParams.id, updatedProduct);

		return {
			message: "Updated Products Succesfully"
		};

	// } else {
		// return {
			// message: "Only admins can update the product details."
		// };

	// };

};

module.exports.archiveProduct = async (reqParams, data) => {

	// if (data.isAdmin) {

		// let updateActiveField = {
		// 	isActive: false

		// };

		 await Product.findByIdAndUpdate(reqParams, data) // id updateActiveField

			// if (error) {

				// return false;

			// } else {

				return {

					message: "archive Product succesfully"
				};
				
			 };

		
	// };
// };



module.exports.activateProduct = (reqParams, data) => {

	if (data.isAdmin) {

		let updateActiveField = {
			isActive: true

		};

		return Product.findByIdAndUpdate(reqParams.id, updateActiveField).then((product, error) => {

			if (error) {

				return false;

			} else {

				return {

					message: "activate Product succesfully"
				};
				
			};

		});
	};
};



// module.exports.archiveProduct = async () => {

// 	 let result = await Product.find({ isActive: false })
// 		return result;
	
// };



