//  User Schema

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    // firstName: {
	// 	type: String,
	// 	required: [true, "First name is required"]
	// },

	// lastName: {
	// 	type: String,
	// 	required: [true, "Last name is required"]
	// },

	email: {
		type: String,
		required: [true, "email is required"]
	},

	password: {
		type: String,
		required: [true, "password is required"]
	},
	orders : [{
		orderId : {
			type : String,
			required : [true, "Order ID is required"]
		},
		products : [{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			name : {
				type : String,
				required : [true, "Product Name is required"]
			},
			price : {
				type : Number,
				required : [true, "Price is required"]
			}
		}],
		totalAmount : {
			type : Number,
			required : [true, "Total Amount is required"]
		},
		purchasedOn : {
			type : Date,
			default : new Date()
		}
	}],

	isAdmin : {
		type: Boolean,
		default : false
	},

	// mobileNo: {
	// 	type: String,
	// 	required: [true, "Mobile No is required"]
	// },

});


module.exports = mongoose.model("User", userSchema);