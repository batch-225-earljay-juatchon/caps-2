const express = require("express");
const router = express.Router();
const auth = require("../auth");
//  Import
const userController = require("../controllers/userController");

//  Route for registration
router.post("/userRegistration", (req, res) => {
    userController.userRegister(req.body).then(result => res.send(result));
});

//  Route for user authentication

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result));
})

 // Route for user Details
router.get("/userDetails", auth.verify, async (req, res) => { // auth.verify, getUser/details

    const userId = auth.decode(req.headers.authorization).id

    let result = await userController.retrieveUserById(userId);
    res.send(result);
})
router.post("/checkUser", async (req, res) => {
    let result = await userController.checkUser(req.body);
    res.send(result);
})
// router.get("/details", auth.verify, (req, res) => {
// // Provides the user's ID for the getProfile controller method

//     const userData = auth.decode(req.headers.authorization)
//     console.log(userData)
//     console.log(req.headers.authorization)

//     userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
// });


module.exports = router;