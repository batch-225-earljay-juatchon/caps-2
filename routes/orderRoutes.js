const express = require("express");
const router = express.Router();
const auth = require("../auth")


const orderController = require("../controllers/orderController");


//  Router for Checkout order
// create order directly

// router.post('/checkOut', auth.verify, (req, res) => {
//     const data = {
//       order: req.body,
//       isAdmin: auth.decode(req.headers.authorization).isAdmin
//     };
//     const userId = auth.decode(req.headers.authorization).id;
//     orderController.checkoutProduct(req.body, userId, data).then((result) => {
//       res.send(result)
//     });
//   });

// module.exports = router;


router.post("/createOrder/:id", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
		quantity : request.body.quantity
	}

	orderController.createOrder(request.params.id,data).then(result => response.send(result));

});

// get current order
router.get("/getCheckOut/:id", auth.verify, async( request, response) => {

	let data = {
		userId : auth.decode(request.headers.authorization).id
	};

	try {
		const result = await orderController.getCheckOut(request.params.id, data);
		response.send(result)
	} catch (error) {
		return console.error(error) //handles error
	}
})

router.put("/checkout/:id", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
	}

	orderController.checkOut(request.params.id, data).then(result => {
		response.send(result);
	})
})


module.exports = router;