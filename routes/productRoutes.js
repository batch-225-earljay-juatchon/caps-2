const express = require("express");
const router = express.Router();
const auth = require("../auth")

const productController = require("../controllers/productController");

//  Router for creating a Product
router.post("/addProduct", (req, res) => { //auth, verify

	const data = {
		product: req.body,
		// isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result => res.send(result));
})

//  Router for Retrive all product
router.get("/retrieveAllProducts", (req, res) => { //auth, verify
 
	const data = {
		product: req.body,
		 // isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.retrieveAllProducts(data).then(result => res.send(result));

})

// 
//  Router for Retrieve all Active product

router.get("/retrieveAllActiveProducts", (req, res) => {

	productController.retrieveAllActiveProducts().then(result => res.send(result));


})

//  Router for Retrieve Single product
router.get("/retrieveSingleProduct/:id", (req, res) => { // auth.verify,

	const productId = req.params.id

	productController.retrieveSingleProduct(productId).then(result => res.send(result));
})

//  Router for Update product
router.put("/updateProduct/:id", (req, res) => { // auth.verify,

	const data = {
		product: req.body,
		// isAdmin: auth.decode(req.headers.authorization).isAdmin

	}
	productController.updateProduct(req.params, req.body, data).then(result => res.send(result));


})

//  Router for Archive product
router.put("/archiveProduct/:id", (req, res) => { //auth.verify,
	const productId = req.params.id
	const data = {
		// product: req.body,
		isActive: false
		// isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.archiveProduct(productId, data).then(result => res.send(result)); //req.params

})

//  Router for Activate product
router.put("/activateProduct/:id", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.activateProduct(req.params, data).then(result => res.send(result));

})

//  Archive Products
router.get("/archiveProduct", (req, res) => {

	productController.archiveProduct().then(result => res.send(result));

})




module.exports = router;